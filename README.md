03.07.2023 - 03.09.2023. 

Erasmus+ Summer Internship

“Germline mutations analysis for breast cancer”

Author: Aleksandra Czerkowicz

Supervisor: Carsten Daub, PhD, Associate Proffesor

Abstract:
Breast cancer is a multifaceted disease influenced by genetic and environmental factors. Inherited mutations are significant contributors to breast cancer susceptibility, with unique founder mutations seen in specific populations like Saudi Arabian women. This project investigates germline mutations associated with breast cancer among Saudi Arabian women, aiming to identify potential risk-contributing mutations. Whole Genome Sequencing and Spatial Transcriptomics data from tumor and healthy tissue of 5 patients were analyzed using the nf-core: sarek pipeline and Manta tool's diploid model. Structural variants were detected in all 5 patients. Patient similarity based on mutation profiles indicated potential associations with specific breast cancer types. The prevalence of large variants warrants further investigation. The X chromosome exhibited significant variant clustering, necessitating future gene-specific analysis. Although shared variants were considered, individual patient relationships require exploration. PTPN2 gene variants predominated in certain patients, while others exhibited lower variant counts. 
Spatial transcriptomics revealed low TYRO3 expression, suggesting functional consequences.
Identical TYRO3 gene variants across patients may indicate population-specific variants. 
Deletions causing Nonsense Mediated mRNA decay (NMDs) impact TYRO3 gene expression, potentially affecting cellular function. 
Further research is advised to validate these findings and uncover their clinical implications.
